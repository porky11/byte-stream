mod lib;

use byte_stream::default_settings::{NativeSettings, PortableSettings};
use lib::assert_streamed_eq;

use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet, LinkedList, VecDeque};

macro_rules! impl_test {
    ($name: ident, $t: ty, $settings: ty) => {
        #[test]
        fn $name() {
            assert_streamed_eq::<$settings, $t>(&vec![1, 2, 3, 4, 5].into_iter().collect());
        }
    };
}

macro_rules! impl_map_test {
    ($name: ident, $t: ty, $settings: ty) => {
        #[test]
        fn $name() {
            assert_streamed_eq::<$settings, $t>(
                &vec![(1, 1.0), (2, 2.0), (3, 3.0), (4, 4.0), (5, 5.0)]
                    .into_iter()
                    .collect(),
            );
        }
    };
}

impl_test!(check_vec_native, Vec<u32>, NativeSettings);
impl_test!(check_vec_portable, Vec<u32>, PortableSettings);

impl_test!(check_vec_deque_native, VecDeque<u32>, NativeSettings);
impl_test!(check_vec_deque_portable, VecDeque<u32>, PortableSettings);

impl_test!(check_linked_list_native, LinkedList<u32>, NativeSettings);
impl_test!(
    check_linked_list_portable,
    LinkedList<u32>,
    PortableSettings
);

impl_test!(check_hash_set_native, HashSet<u32>, NativeSettings);
impl_test!(check_hash_set_portable, HashSet<u32>, PortableSettings);

impl_test!(check_binary_tree_set_native, BTreeSet<u32>, NativeSettings);
impl_test!(
    check_binary_tree_set_portable,
    BTreeSet<u32>,
    PortableSettings
);

impl_map_test!(check_hash_map_native, HashMap<u32, f64>, NativeSettings);
impl_map_test!(check_hash_map_portable, HashMap<u32, f64>, PortableSettings);

impl_map_test!(check_binary_tree_map_native, BTreeMap<u32, f64>, NativeSettings);
impl_map_test!(
    check_binary_tree_map_portable,
    BTreeMap<u32, f64>,
    PortableSettings
);
