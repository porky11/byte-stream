use byte_stream::{from_stream, to_stream, Error, FromStream, ToStream};

use std::fmt::Debug;

pub fn try_streaming<S, V: ToStream<S> + FromStream<S>>(value: &V) -> Result<V, Error> {
    let mut bytes = Vec::new();
    to_stream(value, &mut &mut bytes)?;
    from_stream(&mut &bytes[..])
}

pub fn assert_streamed_eq<S, V: ToStream<S> + FromStream<S> + PartialEq + Debug>(value: &V) {
    let copy = try_streaming(value).unwrap();
    assert_eq!(value, &copy);
}
