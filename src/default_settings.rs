use crate::{
    collections::{Size, SizeSettings},
    numbers::{Endian, EndianSettings},
};

pub struct NativeSettings;
pub struct PortableSettings;

impl EndianSettings for NativeSettings {
    const ENDIAN: Endian = Endian::Native;
}

impl EndianSettings for PortableSettings {
    const ENDIAN: Endian = Endian::Little;
}

impl SizeSettings for NativeSettings {
    const SIZE: Size = Size::Usize;
}

impl SizeSettings for PortableSettings {
    const SIZE: Size = Size::U32;
}
