use crate::{Error, FromStream, ToStream};

use std::io::{Read, Write};

pub enum Endian {
    Little,
    Big,
    Native,
}

pub trait EndianSettings: Sized {
    const ENDIAN: Endian;
}

pub struct LittleEndian;
pub struct BigEndian;
pub struct NativeEndian;

impl EndianSettings for LittleEndian {
    const ENDIAN: Endian = Endian::Little;
}

impl EndianSettings for BigEndian {
    const ENDIAN: Endian = Endian::Big;
}

impl EndianSettings for NativeEndian {
    const ENDIAN: Endian = Endian::Native;
}

macro_rules! impl_num {
    ($t: ty) => {
        impl<S: EndianSettings> ToStream<S> for $t {
            fn to_stream<W: Write>(&self, stream: &mut W) -> Result<(), Error> {
                use Endian::*;
                let bytes = match S::ENDIAN {
                    Little => self.to_le_bytes(),
                    Big => self.to_be_bytes(),
                    Native => self.to_ne_bytes(),
                };
                if stream.write(bytes.as_ref()).is_err() {
                    Err(Error::Stream)
                } else {
                    Ok(())
                }
            }
        }

        impl<S: EndianSettings> FromStream<S> for $t {
            fn from_stream<R: Read>(stream: &mut R) -> Result<Self, Error> {
                const SIZE: usize = std::mem::size_of::<$t>();
                let mut bytes = [0; SIZE];

                if stream.read(&mut bytes).is_err() {
                    Err(Error::Stream)
                } else {
                    use Endian::*;
                    Ok(match S::ENDIAN {
                        Little => Self::from_le_bytes(bytes),
                        Big => Self::from_be_bytes(bytes),
                        Native => Self::from_ne_bytes(bytes),
                    })
                }
            }
        }
    };
    ($t: ty, $($rest: ty),+) => {
        impl_num!($t);
        impl_num!($($rest),+);
    };
}

impl_num!(u8, u16, u32, u64, u128);
impl_num!(i8, i16, i32, i64, i128);
impl_num!(f32, f64);
impl_num!(usize);
