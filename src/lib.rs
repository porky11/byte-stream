pub mod collections;
pub mod numbers;

use std::io::{Read, Write};

#[derive(Debug)]
pub enum Error {
    Stream,
}

pub trait ToStream<S> {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<(), Error>;
}

pub trait FromStream<S>: Sized {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self, Error>;
}

#[inline(always)]
pub fn to_stream<S, V: ToStream<S>, W: Write>(value: &V, stream: &mut W) -> Result<(), Error> {
    value.to_stream(stream)
}

#[inline(always)]
pub fn from_stream<S, V: FromStream<S>, R: Read>(stream: &mut R) -> Result<V, Error> {
    V::from_stream(stream)
}

pub mod default_settings;
